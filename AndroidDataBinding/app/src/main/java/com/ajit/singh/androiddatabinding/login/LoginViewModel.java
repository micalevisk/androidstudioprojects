package com.ajit.singh.androiddatabinding.login;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.Editable;
import android.util.Log;

import com.ajit.singh.androiddatabinding.BR;
import com.ajit.singh.androiddatabinding.util.TextWatcherAdapter;

public class LoginViewModel extends BaseObservable {
	private String username;
	private String password;
	private String loginMessage;
	private boolean validContent;

	public TextWatcherAdapter watcher;


	public LoginViewModel(){
		this(false);
	}

	public LoginViewModel(boolean asRoot){
		if(asRoot){
			//NOTE bad access
			this.username = LoginPresenter.ROOT_LOGIN;
			this.password = LoginPresenter.ROOT_PASSW;
		}

		watcher = new TextWatcherAdapter() {
			@Override public void afterTextChanged(Editable s) {
				if(!(username == null || password == null)) {
					validContent = !username.isEmpty() && !password.isEmpty();
					notifyPropertyChanged(BR.validContent);
				}
			}
		};
	}



	@Bindable
	public boolean isValidContent(){
		return  validContent;
	}


	@Bindable
	public String getUsername() {
		return username;
	}

	@Bindable
	public String getPassword() {
		return password;
	}

	@Bindable
	public String getLoginMessage() {
		return loginMessage;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void loginSucceeded() {
		Log.e("login","loginSucceeded");
		loginMessage = "Login Successful!!" + " as " + username;
		notifyPropertyChanged(BR.loginMessage);
	}

	public void loginFailed() {
		loginMessage = "Login Failed!!";
		notifyPropertyChanged(BR.loginMessage);
	}




}
