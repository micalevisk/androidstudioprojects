package com.ajit.singh.androiddatabinding.login;

import android.util.Log;

public class LoginPresenter {

	public static final String ROOT_LOGIN = "root";
	public static final String ROOT_PASSW = "root";


	public void login(LoginViewModel viewModel) {
		Log.e("login","login");
		if (viewModel.getUsername().equals(ROOT_LOGIN)
				&& viewModel.getPassword().equals(ROOT_PASSW))
			viewModel.loginSucceeded();
		else
			viewModel.loginFailed();
	}
}
