// (c) https://github.com/ajitsing/AndroidDataBinding
package com.ajit.singh.androiddatabinding.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.databinding.DataBindingUtil;
import com.ajit.singh.androiddatabinding.R;
import com.ajit.singh.androiddatabinding.databinding.ActivityLoginBinding; // classe Binding que foi gerada (baseada no layout activity_login.xml)

public class LoginActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
		binding.setLoginViewModel(new LoginViewModel());
		binding.setHandler(new LoginHandler(new LoginPresenter()));



		//NOTE set activity_login and password
//		LoginViewModel user = new LoginViewModel(true);
//		binding.setLoginViewModel(user);
	}
}
