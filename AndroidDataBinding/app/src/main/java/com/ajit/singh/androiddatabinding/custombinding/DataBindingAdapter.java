// (c) https://github.com/ajitsing/AndroidDataBinding
// https://developer.android.com/topic/libraries/data-binding/index.html?hl=pt-br#build_environment
// http://www.vogella.com/tutorials/AndroidDatabinding/article.html#android-data-binding

package com.ajit.singh.androiddatabinding.custombinding;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class DataBindingAdapter {
	@BindingAdapter("toast")
	public static void showToast(View view, String toast) {
		Log.e("Login","showToast");
		if (toast != null && !toast.isEmpty());
			Toast.makeText(view.getContext(), toast, Toast.LENGTH_SHORT).show();
	}
}