## conteúdos para o trabalho

- [ ] [Android Material Design Snackbar Example](http://www.androidhive.info/2015/09/android-material-design-snackbar-example/)
- [ ] [SNACKBAR ANDROID LIBRARY](http://williammora.com/snackbar-android-library)
- [ ] [Bottom navigation](https://material.io/guidelines/components/bottom-navigation.html#bottom-navigation-behavior)
- [ ] [Getting Started with Data Binding in Android](http://www.opgenorth.net/blog/2015/09/15/android-data-binding-intro/)
- [ ] [Using the App Toolbar](https://guides.codepath.com/android/Using-the-App-ToolBar)
- [ ] [Introduction to Coordinator Layout on Android](https://lab.getbase.com/introduction-to-coordinator-layout-on-android/)
- [ ] [Android SQLite Database with Multiple Tables](http://www.androidhive.info/2013/09/android-sqlite-database-with-multiple-tables/)

+ [Material Design Palette](https://www.materialpalette.com/)	```visualizar combinação de cores```
+ [Material Design Icons](https://materialdesignicons.com/) 	```baixar os ícones de menus/itens```
+ [Material Icons](https://material.io/icons/)


## TODO
+ Fazer rascunho das telas principais (sem ações/ícones/cores corretas):
	- (main) Login = ___Login Activity___
	- (secundária) Gerenciador = ___Basic Activity___
	- (terciária) Opções = ___Navigation Drawer Activity___
