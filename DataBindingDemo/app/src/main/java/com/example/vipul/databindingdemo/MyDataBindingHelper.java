package com.example.vipul.databindingdemo;

/**
 * Created by vipul on 1/31/2016.
 */
public class MyDataBindingHelper {
    private String salutation;

    public MyDataBindingHelper(String salutation) {this.salutation = salutation;}

    public static MyDataBindingHelper get(String salutation) {return new MyDataBindingHelper(salutation);}

    public String getMessage() {return String.format("Hi %s! :)", salutation);}
}
