/// with RecyclerView vide https://www.youtube.com/watch?v=hc6pJUEgzj8 | https://developer.android.com/training/material/lists-cards.html?hl=pt-br | https://www.youtube.com/watch?v=PMX7zRD3dhg
/// (c) http://www.developer.com/ws/android/programming/how-to-use-android-data-binding.html
package com.example.vipul.databindingdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import android.databinding.DataBindingUtil;
import com.example.vipul.databindingdemo.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        TextView unbound = (TextView) findViewById(R.id.textViewUnBound);
        MyDataBindingHelper myDataBindingHelper = MyDataBindingHelper.get("Unbound");
        unbound.setText(myDataBindingHelper.getMessage());

        // (c) http://stackoverflow.com/questions/35883452/cant-resolve-android-databinding-class | http://stackoverflow.com/questions/31365692/android-databinding-how-and-when-the-binding-classes-will-be-generated
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setMydatabindinghelper(MyDataBindingHelper.get("Bound"));

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) return true;

        return super.onOptionsItemSelected(item);
    }
}