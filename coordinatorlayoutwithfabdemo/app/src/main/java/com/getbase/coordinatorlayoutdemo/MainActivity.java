// (c) https://github.com/ggajews/coordinatorlayoutwithfabdemo
package com.getbase.coordinatorlayoutdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Snackbar
						.make(v, "Alternar Activity?", Snackbar.LENGTH_LONG)
						.setAction("SIM", new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								startActivity(new Intent(MainActivity.this, Compose.class));
							}
						})
						.show();
			}
		});

	}



}
