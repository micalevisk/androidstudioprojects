// (c) https://github.com/ajitsing/AlarmManagerAndReceiver
// https://gist.github.com/BrandonSmith/6679223
package bootcamp.android.demoapp;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuItem;


import java.util.Calendar;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//		initAlarmService();
    }




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case R.id.action_3:
				scheduleNotification( getNotification("3 second delay"), 3000 );
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void scheduleNotification(Notification notification, int delay) {
		Intent notificationIntent = new Intent(this, AlarmReceiver.class);
		notificationIntent.putExtra(AlarmReceiver.NOTIFICATION_ID, 1);
		notificationIntent.putExtra(AlarmReceiver.NOTIFICATION, notification);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		long futureInMillis = SystemClock.elapsedRealtime() + delay;
		AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
	}

	private Notification getNotification(String content) {
		Notification.Builder builder = new Notification.Builder(this);
		builder.setContentTitle("Scheduled Notification");
		builder.setContentText(content);
		builder.setSmallIcon(R.mipmap.ic_launcher);
		return builder.build();
	}



	//// NOTIFICATION WITH ACTION ON CLICK
	private void initAlarmService(){
		AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

		Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
		notificationIntent.addCategory("android.intent.category.DEFAULT");

		PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 15);
		alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
	}
}
