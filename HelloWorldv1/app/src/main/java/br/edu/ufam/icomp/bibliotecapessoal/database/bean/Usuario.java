package br.edu.ufam.icomp.bibliotecapessoal.database.bean;

import java.io.Serializable;

import br.edu.ufam.icomp.bibliotecapessoal.database.BancoDeDadosHelper;

/**
 * Representar um usuario no banco de dados e contém todos os seus dados relacionados a ele. (Modelo)
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class Usuario implements Serializable {

	private String nick;
	private String senha;

	// TODO adicionar atributos relacionados aos demais dados úteis.


	public Usuario(String nick, String senha){
		this.nick = nick;
		this.senha = BancoDeDadosHelper.MD5( senha );
	}


	/**********************************************************************************************/

	/////////////////////////// [ AUXILIARES ] ///////////////////////////


	/////////////////////////// [ GETTERS ] ///////////////////////////
	public String getNick(){ return nick; }

	public String getSenha(){ return  senha; }

	/////////////////////////// [ SETTERS ] ///////////////////////////
	public void setNick(String nick){ this.nick = nick; }


}
