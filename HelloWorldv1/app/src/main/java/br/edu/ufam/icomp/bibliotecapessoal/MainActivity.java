/*******************************************************************************
 * Copyright (c) 17/01/17 10:47 - Micael Levi L. Cavalcante
 ******************************************************************************/

package br.edu.ufam.icomp.bibliotecapessoal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Usuario;
import br.edu.ufam.icomp.bibliotecapessoal.login.LoginPresenter;
import br.edu.ufam.icomp.bibliotecapessoal.login.LoginViewModel;
import br.edu.ufam.icomp.bibliotecapessoal.R;
import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;

import android.databinding.DataBindingUtil;
import android.widget.EditText;
import android.widget.Toast;

import br.edu.ufam.icomp.bibliotecapessoal.databinding.ActivityMainBinding; // classe Binding que foi gerada (baseada no layout activity_login.xml)
import br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.DrawerMainActivity;
import br.edu.ufam.icomp.bibliotecapessoal.util.InputFilterAllLowerNoSpaces;
import br.edu.ufam.icomp.bibliotecapessoal.util.StringUtils;


public class MainActivity extends AppCompatActivity {

	ActivityMainBinding mainBinding;

	///////// View /////////
	private CheckBox cbxManterConexao;
	private AlertDialog alertDialogSobre;

	//////// Extras ////////
	Usuario ultimoUsuarioLogado;
	private LoginViewModel loginViewModel;

	private LoginPresenter loginPresenter;


	/**********************************************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

		ultimoUsuarioLogado = null;
		loginViewModel = new LoginViewModel(getResources(), mainBinding);
		loginPresenter = new LoginPresenter(this);

		mainBinding.setModel(loginViewModel);
		mainBinding.setPresenter(loginPresenter);
		mainBinding.setMainActivity(this);

		this.cbxManterConexao = mainBinding.cbxManterConexao;
		this.alertDialogSobre = Constantes.initDialogSobre(this);
    }

	@Override
	protected void onResume() {
		super.onResume();
		recarregarWidgets();
	}

	@Override
	protected void onPause() {
		super.onPause();
		salvarPreferenciasCompartilhadas();
	}


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.first_menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
		/// TODO ações para as opções de menu
        switch (item.getItemId()){
            case R.id.itemSobre: mostrarSobre(); return true;
			case R.id.itemNovoUsuario: mostrarDialogCadastro(); return true;
            default: return super.onOptionsItemSelected(item);
        }
    }


    /**********************************************************************************************/

	public void fazerLogin(boolean force){
		Log.i("rastrear","fazerLogin");
		this.ultimoUsuarioLogado = loginPresenter.login(loginViewModel, force); // recupera as outras informações
		if (ultimoUsuarioLogado != null) chamarTelaPrincipal();
	}

	public boolean cadastrarUsuario(String nick, String senha){
		this.ultimoUsuarioLogado = new Usuario(nick, senha);
		/// TODO: 25/01/2017 chamar cadastro no BD
		if( loginPresenter.cadastrar(ultimoUsuarioLogado) ){

			return true;
		}
		else{
			/// TODO: 26/01/2017 mostrar Toast de erro/falha ao cadastrar
			this.ultimoUsuarioLogado = null;
			return false;
		}
	}

    /**********************************************************************************************/


	///////////////////////// [ EXTRAS ] /////////////////////////
	public void recarregarWidgets(){
		Log.i("rastrear","recarregarWidgets");
		loginViewModel.reset();
		carregarPreferenciasCompartilhadas();
	}


	private void salvarPreferenciasCompartilhadas(){
		Log.i("rastrear","salvarPreferenciasCompartilhadas");

		getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE)
			.edit()
			.putBoolean( Constantes.CHECKBOX_MANTERCONEXAO, this.cbxManterConexao.isChecked() )
			.commit();

	}

	private void carregarPreferenciasCompartilhadas(){
		Log.i("rastrear","carregarPreferenciasCompartilhadas");
		SharedPreferences sharedPref = this.getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE);

		boolean default_cbxManterConexao = getResources().getBoolean(R.bool.default_cbxManterConexao);
		this.cbxManterConexao.setChecked( sharedPref.getBoolean( Constantes.CHECKBOX_MANTERCONEXAO, default_cbxManterConexao ));

		if(cbxManterConexao.isChecked()){
			String nickUltimoLogado = sharedPref.getString(Constantes.ULTIMO_LOGADO, null); // recupera o nick do último usuário logado
			if(nickUltimoLogado != null) {
				loginViewModel.setUsername(nickUltimoLogado);
				/// NOTE a linha abaixo é desnecessária?
				loginViewModel.setPassword(""); // apenas para não causar erro na verificação do
				fazerLogin(true);
			}
		}
	}



    ///////////////////////// [ AÇÕES ] /////////////////////////
    private void mostrarSobre(){
        this.alertDialogSobre.show();
    }

    public void chamarTelaPrincipal(){
		Log.i("rastrear", "chamarTelaPrincipal");
		if(ultimoUsuarioLogado == null) return;
		Intent intent = new Intent(getApplicationContext(), DrawerMainActivity.class);
		intent.putExtra(Constantes.ULTIMO_LOGADO, this.ultimoUsuarioLogado);
		startActivity(intent);
    }


	public void mostrarDialogCadastro(){
		Log.i("rastrear", "mostrarDialogCadastro");

		final InputFilter[] filtroCredenciais = new InputFilter[] { new InputFilterAllLowerNoSpaces() };

		final View viewCadastro = getLayoutInflater().inflate(R.layout.dialog_cadastro, null);
		final EditText novoNick = (EditText) viewCadastro.findViewById(R.id.etNovoNick);
		final EditText novaSenha = (EditText) viewCadastro.findViewById(R.id.etNovaSenha);
		novoNick.setFilters(filtroCredenciais);
		novaSenha.setFilters(filtroCredenciais);

		AlertDialog.Builder cadastroBuilder = new AlertDialog.Builder(MainActivity.this)
				.setView(viewCadastro)
				.setNegativeButton(getResources().getString(R.string.txtCancelar), null)
				.setPositiveButton(getResources().getString(R.string.txtCadastrar), null);

		final AlertDialog dialogCadastro = cadastroBuilder.create();
		dialogCadastro.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
				button.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						String nick = novoNick.getText().toString().trim();
						String senha = novaSenha.getText().toString().trim();

						if (!nick.isEmpty() && !senha.isEmpty()) {
							if( cadastrarUsuario(nick, senha) ){
								dialogCadastro.dismiss();
								chamarTelaPrincipal();
							}
							else Toast.makeText(MainActivity.this, StringUtils.capitalize( getResources().getString(R.string.msgNickJaCadastrado) ), Toast.LENGTH_SHORT).show();
						}
						else Toast.makeText(MainActivity.this, StringUtils.capitalize( getResources().getString(R.string.msgPreenchaOsDoisCampos) ), Toast.LENGTH_SHORT).show();
					}

				});
			}
		});
		dialogCadastro.show();

	}

}