package br.edu.ufam.icomp.bibliotecapessoal.login;


import android.content.Context;
import android.util.Log;

import br.edu.ufam.icomp.bibliotecapessoal.database.BancoDeDadosHelper;
import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Usuario;
import br.edu.ufam.icomp.bibliotecapessoal.database.dao.UsuarioDAO;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class LoginPresenter {

	private Context context;
	LoginViewModel viewModel;

	private UsuarioDAO usuarioDAO;

	public LoginPresenter(Context context){
		this.usuarioDAO = new UsuarioDAO(context);
		this.context = context;
	}

	/**
	 * Acessa os atributos {@link LoginViewModel#username} e {@link LoginViewModel#password}
	 * para verificar se existe no banco de dados.
	 * @param viewModel - Armazena os campos valores (String) de login e senha da Activity principal.
	 * @param dontCheckCredentials - Se for <code>TRUE</code>, verifica apenas existência do nick no BD.
	 * @return Cria um {@linkplain Usuario} baseado no login e senha (sem os demais dados).
	 */
	public Usuario login(LoginViewModel viewModel, boolean dontCheckCredentials){
		Log.i("rastrear","login" );

		String username = viewModel.getUsername();
		String password = BancoDeDadosHelper.MD5( viewModel.getPassword() );

		Usuario userFromDB = new UsuarioDAO(context).getUsuario(username,password,dontCheckCredentials);

		if(userFromDB != null){
			/// TODO carregar todos os dados úteis do usuario logado (admitir que pode ser conta nova)
			viewModel.loginSucceeded();
		}
		else{
			viewModel.loginFailed();
		}

		return userFromDB;
//		return viewModel.logar( userFromDB );
	}


	public boolean cadastrar(Usuario u){
		Log.i("rastrear","cadastrar");
		if( usuarioDAO.addUsuario(u) ){
			/// TODO: 26/01/2017 inserir no BD e carregar os dados úteis para o 'u'
			Log.i("rastrear","cadastrado com sucesso!!");
			return true;
		}
		return false;
	}




}
