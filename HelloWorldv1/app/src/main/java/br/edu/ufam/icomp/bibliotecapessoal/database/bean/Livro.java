package br.edu.ufam.icomp.bibliotecapessoal.database.bean;

import java.io.Serializable;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 10/02/2017
 */
public class Livro implements Serializable {

	private int rowid;
	private String usuarios_nick;
	private boolean desejado;
	private boolean favorito;
	private String nome;
	private String autor;
	private String descricao;
	private int edicao;
	private String categoria;
	private String dia_devolucao;

	/**
	 *
	 * @param rowid
	 * @param usuarios_nick
	 * @param desejado
	 * @param favorito
	 * @param nome
	 * @param autor
	 * @param descricao
	 * @param edicao
	 * @param categoria
	 * @param dia_devolucao
	 */
	public Livro(int rowid, String usuarios_nick, boolean desejado, boolean favorito, String nome, String autor, String descricao, int edicao, String categoria, String dia_devolucao){
		this.rowid = rowid;
		this.usuarios_nick = usuarios_nick;
		this.desejado = desejado;
		this.favorito = favorito;
		this.nome = nome;
		this.autor = autor;
		this.descricao = descricao;
		this.edicao = edicao;
		this.categoria = categoria;
		this.dia_devolucao = dia_devolucao;
	}

	/**********************************************************************************************/

	/////////////////////////// [ AUXILIARES ] ///////////////////////////


	/////////////////////////// [ GETTERS ] ///////////////////////////
	public int getRowid() {
		return rowid;
	}

	public String getUsuarios_nick() {
		return usuarios_nick;
	}

	public boolean isDesejado() {
		return desejado;
	}

	public boolean isFavorito() {
		return favorito;
	}

	public String getNome() {
		return nome;
	}

	public String getAutor() {
		return autor;
	}

	public String getDescricao() {
		return descricao;
	}

	public int getEdicao() {
		return edicao;
	}

	public String getCategoria() {
		return categoria;
	}


	public String getDia_devolucao() {
		return dia_devolucao;
	}


	/////////////////////////// [ SETTERS ] ///////////////////////////
	public void setRowid(int rowid) {
		this.rowid = rowid;
	}

	public void setUsuarios_nick(String usuarios_nick) {
		this.usuarios_nick = usuarios_nick;
	}

	public void setDesejado(boolean desejado) {
		this.desejado = desejado;
	}

	public void setFavorito(boolean favorito) {
		this.favorito = favorito;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setEdicao(int edicao) {
		this.edicao = edicao;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public void setDia_devolucao(String dia_devolucao) {
		this.dia_devolucao = dia_devolucao;
	}
}
