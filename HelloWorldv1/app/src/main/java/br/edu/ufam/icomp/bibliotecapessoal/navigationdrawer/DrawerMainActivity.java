package br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Livro;
import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Usuario;
import br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.fragments.CompradosFragment;
import br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.fragments.EmprestadosFragment;
import br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.fragments.FavoritosFragment;
import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;
import br.edu.ufam.icomp.bibliotecapessoal.R;
import br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.fragments.DesejadosFragment;

public class DrawerMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

	Usuario usuarioLogado;

	///////// EXTRAS /////////
	private AlertDialog alertDialogConfirmarLogout;
	private AlertDialog alertDialogSobre;
	private boolean manterLogado;


	// §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ TESTE: PARA PREENCHER O RECYCLERVIEW
	public List<Livro> getSetLivroList(int qtd){
		String[] nomes = new String[]{ "Gambardella",  "Ralls",  "Corets",  "Corets",  "Corets",  "Randall",  "Thurman",  "Knorr",  "Kress",  "O'Brien",  "O'Brien",  "Galos" };
		String[] autores = new String[]{ "Matthew",  "Kim",  "Eva",  "Eva",  "Eva",  "Cynthia",  "Paula",  "Stefan",  "Peter",  "Tim",  "Tim",  "Mike" };
		String[] descricoes = new String[]{ "XMLDeveloper'sGuide",  "MidnightRain",  "MaeveAscendant",  "Oberon'sLegacy",  "TheSunderedGrail",  "LoverBirds",  "SplishSplash",  "CreepyCrawlies",  "ParadoxLost",  "Microsoft.NET:TheProgrammingBible",  "MSXML3:AComprehensiveGuide",  "VisualStudio7:AComprehensiveGuide" };

		List<Livro> listLivros = new ArrayList<>(qtd);

		for(int i=0; i < qtd; ++i){
			Livro l = new Livro(i, usuarioLogado.getNick(), false, false, nomes[i%nomes.length], autores[i%autores.length], descricoes[i%descricoes.length], 1, "didático", null);
			listLivros.add(l);
		}

		return listLivros;
	}
	// §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§



	/// TODO: 01/02/2017 implementar ação no botão flutuante
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_drawer);


		SharedPreferences sharedPref = this.getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE);
		this.manterLogado = sharedPref.getBoolean( Constantes.CHECKBOX_MANTERCONEXAO, false ); // recupera o estado da checkbox "manter-me conectado"
		Log.e("rastrear", "RECUPERADO NA PRIMEIRA TELA:"+manterLogado);

		init();
	}


	@Override
	public void onBackPressed() {
		realizarLogout(false);
	}



	/// TOOLBAR
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.second_menu_toolbar, menu);

		MenuItem itemBuscar = menu.findItem(R.id.itemBuscar);
		final SearchView searchView = (SearchView)itemBuscar.getActionView();
		searchView.setInputType(InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				String autor = query.trim().toLowerCase();
				if(!autor.isEmpty()) {
					searchView.clearFocus();
					filtrarPorAutor(autor);
				}
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) { return false; }
		});


		return super.onCreateOptionsMenu(menu);
	}


	/// TODO: 01/02/2017 implementar ações para trocar de fragment
	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		int id = item.getItemId();

		/// Itens relacioandos aos livros
		if (id == R.id.nav_comprados) 		chamarFragmentComprados();
		else if (id == R.id.nav_emprestados)chamarFragmentEmprestados();
		else if (id == R.id.nav_favoritos) 	chamarFragmentFavoritos();
		else if (id == R.id.nav_desejados) 	chamarFragmentDesejados();

		else if (id == R.id.nav_sobre) this.alertDialogSobre.show();
		else if (id == R.id.nav_sair) realizarLogout(true);

		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}


	/**********************************************************************************************/
	private void init(){
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});


		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
				this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();

		NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		////////////////////////////////////////////////////////////////////////////////////////////
		Intent intent = getIntent();

		/// recupera o usuario passado pela activity de login
		usuarioLogado = (Usuario) intent.getSerializableExtra(Constantes.ULTIMO_LOGADO);
		salvarUltimoLogin(); /// salvar no SharedPreferences se a conexão for persistente; apagar caso contrário
		this.alertDialogConfirmarLogout = initAlertDialogConfirmarLogout();
		this.alertDialogSobre = Constantes.initDialogSobre(this);


		View header=navigationView.getHeaderView(0);
		TextView tvHeaderNick = (TextView) header.findViewById(R.id.tvHeaderNick);
		tvHeaderNick.setText(usuarioLogado.getNick());
	}

	private AlertDialog initAlertDialogConfirmarLogout(){
		Log.i("rastrear","initAlertDialogConfirmarLogout");
		// (c) http://pt.wikihow.com/Exibir-uma-Caixa-de-Di%C3%A1logo-com-AlertDialog-no-Android
		// (c) https://www.tutorialspoint.com/android/android_alert_dialoges.htm
		// (c) http://stackoverflow.com/questions/25504458/alertdialog-with-ok-cancel-buttons
		return new AlertDialog.Builder(this)
				.setTitle( getResources().getString(R.string.tituloConfirmarLogout) + " '" + usuarioLogado.getNick() + "'" )
				.setMessage( getResources().getString(R.string.msgConfirmarLogout) )
				.setNegativeButton( getResources().getString(R.string.confirmacaoNAO), null)
				.setPositiveButton( getResources().getString(R.string.confirmacaoSIM), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						manterLogado = false;
						apagarUltimoLogin();
						salvarUltimoLogin();
						finish(); /// fecha a activity
						//				Toast.makeText(PrimeiraTelaActivity.this,"Você escolheu a opção \"SIM\"",Toast.LENGTH_LONG).show();
						//				android.os.Process.killProcess(android.os.Process.myPid());
						//				System.exit(1);
					}
				})
				.create();
	}




	
	/////////////////////////// [ AUXILIARES ] ///////////////////////////
	private void apagarUltimoLogin(){
		Log.i("rastrear","apagarUltimoLogin");
		getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE)
				.edit()
				.remove(Constantes.ULTIMO_LOGADO) // para evitar o auto-login
				.commit();
	}


	private void salvarUltimoLogin(){
		Log.i("rastrear","salvarUltimoLogin");
		/// TODO salvar login no SharedPreferences se 'nick' for diferente de 'null', caso contrário, apagar ultimoLogin do BD
		SharedPreferences sharedPref = this.getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE);

		if(!manterLogado) apagarUltimoLogin();
		else {
			sharedPref.edit().putString(Constantes.ULTIMO_LOGADO, usuarioLogado.getNick()).commit(); // salva o nick do último usuário logado
		}

	}

	private void realizarLogout(boolean force){
		Log.i("rastrear","realizarLogout");
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if(force) {
			drawer.closeDrawer(GravityCompat.START);
			this.alertDialogConfirmarLogout.show();
			return;
		}

		if (drawer.isDrawerOpen(GravityCompat.START)) drawer.closeDrawer(GravityCompat.START);
		else this.alertDialogConfirmarLogout.show();
	}

	///////////////////////// [ CHAMADORES ] /////////////////////////

	private void chamarFragmentComprados(){
		CompradosFragment compradosFragment = new CompradosFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();

		fragmentManager
				.beginTransaction()
				.replace(R.id.content_main_drawer, compradosFragment, compradosFragment.getTag())
				.commit();
	}

	private void chamarFragmentEmprestados(){
		EmprestadosFragment emprestadosFragment = new EmprestadosFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();

		fragmentManager
				.beginTransaction()
				.replace(R.id.content_main_drawer, emprestadosFragment, emprestadosFragment.getTag())
				.commit();
	}

	private void chamarFragmentFavoritos(){
		FavoritosFragment favoritosFragment = new FavoritosFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();

		fragmentManager
				.beginTransaction()
				.replace(R.id.content_main_drawer, favoritosFragment, favoritosFragment.getTag())
				.commit();
	}

	private void chamarFragmentDesejados(){
		DesejadosFragment desejadosFragment = new DesejadosFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();

		fragmentManager
				.beginTransaction()
				.replace(R.id.content_main_drawer, desejadosFragment, desejadosFragment.getTag())
				.commit();
	}



	///////////////////////// [ AÇÕES ] /////////////////////////


	/// TODO: 02/02/2017 filtrar aqui (o ArrayAdapter(?))
	private void filtrarPorAutor(String nomeAutor){
		Log.i("rastrear", "filtrarPorAutor:"+nomeAutor);
	}

}
