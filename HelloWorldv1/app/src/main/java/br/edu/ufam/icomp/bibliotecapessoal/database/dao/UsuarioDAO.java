package br.edu.ufam.icomp.bibliotecapessoal.database.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Usuario;
import br.edu.ufam.icomp.bibliotecapessoal.database.BancoDeDadosHelper;
import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;

/**
 * CRUD operations for {@link Usuario}
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class UsuarioDAO {
	private SQLiteDatabase bd;

	/// Querys
	private static final String QUERY_SELECT_USUARIO = "SELECT * FROM "+Constantes.TABLE_USUARIOS+" WHERE nick=?";// string:nick
	private static final String QUERY_SELECT_USUARIO_SENHA = "SELECT * FROM "+Constantes.TABLE_USUARIOS+" WHERE nick=? AND senha=?";// string:nick, string:senha
	private static final String QUERY_INSERT_USUARIO = "INSERT INTO usuarios("+Constantes.COL_NICK+", "+Constantes.COL_SENHA +") VALUES(?, ?)";// string:nick, string:senha
	private static final String QUERY_SELECT_USUARIOS = "SELECT rowid AS _id, "+Constantes.COL_NICK+", "+Constantes.COL_SENHA+" FROM "+Constantes.TABLE_USUARIOS+" order by "+Constantes.COL_NICK;

	/**********************************************************************************************/

	public UsuarioDAO(Context context){
		this.bd = (new BancoDeDadosHelper(context)).getWritableDatabase();
	}


	/**
	 * Se o nick estiver no BD, cria um {@link Usuario} a partir do <CODE>nick</CODE> e <CODE>senha</CODE> passados como parâmetro,
	 * senão, retornar <CODE>NULL</CODE>
	 * @param nick
	 * @param senha
	 * @param verificarApenasNick - Se for <CODE>TRUE</CODE> então apenas busca o usuário pelo seu nick.
	 * @return
	 */
	public Usuario getUsuario(String nick, String senha, boolean verificarApenasNick){
		Log.i("rastrear", "getUsuario");
		Usuario usuario = null;
		Cursor cursor;

		if(verificarApenasNick)	cursor = bd.rawQuery(QUERY_SELECT_USUARIO , new String[]{ nick });
		else cursor = bd.rawQuery(QUERY_SELECT_USUARIO_SENHA, new String[]{nick, senha});

		if (cursor.moveToNext()) usuario = new Usuario(cursor.getString(0), cursor.getString(1));

		return usuario;
	}



	public boolean addUsuario(Usuario u){
		Log.i("rastrear","addUsuario");
		try{
			bd.execSQL(QUERY_INSERT_USUARIO, new String[]{ u.getNick(), u.getSenha() });
			return true;
		}catch (SQLException e){
			Log.e(Constantes.DATABASE_LOG_TAG, e.getMessage());
			return false;
		}
	}



	public Cursor getUsuarios(){
		Log.i("rastrear", "getUsuarios");
		return bd.rawQuery(QUERY_SELECT_USUARIOS, null);
	}




}
