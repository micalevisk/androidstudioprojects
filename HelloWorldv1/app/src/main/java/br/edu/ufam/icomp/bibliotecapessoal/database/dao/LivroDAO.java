package br.edu.ufam.icomp.bibliotecapessoal.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import br.edu.ufam.icomp.bibliotecapessoal.database.tabelas.TableLivros;
import br.edu.ufam.icomp.bibliotecapessoal.database.BancoDeDadosHelper;
import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Livro;
import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 10/02/2017
 */
public class LivroDAO {
	private SQLiteDatabase bd;

	/// Querys
	private static final String CMD_cadastrarLivro = "INSERT INTO" + Constantes.TABLE_LIVROS + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";// string:nick, string:nome, string:autor, string:descricao, int:edicao, string:categoria, int:desejado, int:favorito, string:dia_devolucao
	private static final String QUERY_ALTERAR = "UPDATE "+ Constantes.TABLE_LIVROS+ " SET " + Constantes.COL_NOME+ "=?, " + Constantes.COL_AUTOR+ "=?, " + Constantes.COL_DESCRICAO+ "=?, " +Constantes.COL_EDICAO+ "=?, " + Constantes.COL_CATEGORIA+ "=?, " + Constantes.COL_DESEJADO+ "=?, " +Constantes.COL_FAVORITO+ "=?, " + Constantes.COL_DIA_DEVOLUCAO+ "=? " + "WHERE " + Constantes.COL_USUARIOS_NICK+"=?" + " AND rowid=?";

	/**********************************************************************************************/

	public LivroDAO(Context context){
		this.bd = (new BancoDeDadosHelper(context)).getWritableDatabase();
	}

	public void insertLivro(Livro livro){
		ContentValues contentValues = new ContentValues();
		contentValues.put(TableLivros.USUARIOS_NICK, livro.getUsuarios_nick());
		contentValues.put(TableLivros.NOME, livro.getNome());
		contentValues.put(TableLivros.AUTOR, livro.getAutor());
		contentValues.put(TableLivros.DESCRICAO, livro.getDescricao());
		contentValues.put(TableLivros.EDICAO, livro.getEdicao());
		contentValues.put(TableLivros.CATEGORIA, livro.getCategoria());
		contentValues.put(TableLivros.DESEJADO, livro.isDesejado());
		contentValues.put(TableLivros.FAVORITO, livro.isFavorito());
		contentValues.put(TableLivros.DIA_DEVOLUCAO, livro.getDia_devolucao());

		bd.insert(TableLivros.TABLE_NAME, null, contentValues);
	}

}

