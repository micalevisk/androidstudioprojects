package br.edu.ufam.icomp.bibliotecapessoal.custombinding;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class DataBindingAdapter {
	@BindingAdapter("toast")
	public static void showToast(View view, String toast) {
		Log.e("Login","showToast");
		if (toast != null && !toast.isEmpty()){
			Toast.makeText(view.getContext(), toast, Toast.LENGTH_SHORT).show();
		}
	}
}