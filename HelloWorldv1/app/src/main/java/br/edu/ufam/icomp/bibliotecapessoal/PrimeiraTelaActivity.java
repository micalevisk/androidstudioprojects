/*******************************************************************************
 * Copyright (c) 17/01/17 10:47 - Micael Levi L. Cavalcante
 ******************************************************************************/

package br.edu.ufam.icomp.bibliotecapessoal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


import br.edu.ufam.icomp.bibliotecapessoal.R;
import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Usuario;
import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;

@Deprecated
public class PrimeiraTelaActivity extends AppCompatActivity {

	Usuario usuarioLogado;

	///////// EXTRAS /////////
	private AlertDialog alertDialogConfirmarLogout;
	private boolean manterLogado;

    ///////// TextViews /////////
    private TextView textLogin;
    private TextView textSenha;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primeira_tela);

        this.textLogin = (TextView) findViewById(R.id.textLogin2);
        this.textSenha = (TextView) findViewById(R.id.textSenha2);

		SharedPreferences sharedPref = this.getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE);
		this.manterLogado = sharedPref.getBoolean( Constantes.CHECKBOX_MANTERCONEXAO, false ); // recupera o estado da checkbox "manter-me conectado"
		Log.e("rastrear", "RECUPERADO NA PRIMEIRA TELA:"+manterLogado);

		init();
    }

	@Override
	public void onBackPressed() {
		this.alertDialogConfirmarLogout.show();
	}


	/**********************************************************************************************/


	/**********************************************************************************************/
    private void init(){
		Intent intent = getIntent();

		/// recupera o usuario passado pela activity de login
		usuarioLogado = (Usuario) intent.getSerializableExtra(Constantes.ULTIMO_LOGADO);
		salvarUltimoLogin(); /// salvar no SharedPreferences se a conexão for persistente; apagar caso contrário


		findViewById(R.id.fabAdicionar).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				///
			}
		});


		this.alertDialogConfirmarLogout = initAlertDialogConfirmarLogout();

		/// only teste
		setTextLogin(usuarioLogado.getNick());
		setTextSenha(usuarioLogado.getSenha());
    }

	private AlertDialog initAlertDialogConfirmarLogout(){
		// (c) http://pt.wikihow.com/Exibir-uma-Caixa-de-Di%C3%A1logo-com-AlertDialog-no-Android
		// (c) https://www.tutorialspoint.com/android/android_alert_dialoges.htm
		// (c) http://stackoverflow.com/questions/25504458/alertdialog-with-ok-cancel-buttons
		return new AlertDialog.Builder(this)
			.setTitle( getResources().getString(R.string.tituloConfirmarLogout) )
			.setMessage( getResources().getString(R.string.msgConfirmarLogout) )
			.setNegativeButton( getResources().getString(R.string.confirmacaoNAO), null)
			.setPositiveButton( getResources().getString(R.string.confirmacaoSIM), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					manterLogado = false;
					apagarUltimoLogin();
					salvarUltimoLogin();
					finish(); /// fecha a activity
	//				Toast.makeText(PrimeiraTelaActivity.this,"Você escolheu a opção \"SIM\"",Toast.LENGTH_LONG).show();
	//				android.os.Process.killProcess(android.os.Process.myPid());
	//				System.exit(1);
				}
			})
			.create();
	}



    /////////////////////////// [ GETTERS/SETTERS ] ///////////////////////////
    private void setTextLogin(String value){
        this.textLogin.setText(value);
    }

    private  void setTextSenha(String value){
        this.textSenha.setText(value);
    }

	private void apagarUltimoLogin(){
		getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE)
				.edit()
				.remove(Constantes.ULTIMO_LOGADO) // para evitar o auto-login
				.commit();
	}


	/////////////////////////// [ AUXILIARES ] ///////////////////////////
	private void salvarUltimoLogin(){
		Log.i("rastrear","salvarUltimoLogin");
		/// TODO salvar login no SharedPreferences se 'nick' for diferente de 'null', caso contrário, apagar ultimoLogin do BD
		SharedPreferences sharedPref = this.getSharedPreferences(Constantes.FILE_KEY_MAIN_ACTIVITY, Context.MODE_PRIVATE);

		if(!manterLogado) apagarUltimoLogin();
		else sharedPref.edit().putString(Constantes.ULTIMO_LOGADO, usuarioLogado.getNick()).commit(); // salva o nick do último usuário logado

	}

}
