package br.edu.ufam.icomp.bibliotecapessoal.util;

import android.view.View;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 10/02/2017
 */
public interface RecyclerViewOnClickListenerHacker {
	public void onClickListener(View view, int position);
	public void onLongPressClickListener(View view, int position);
}
