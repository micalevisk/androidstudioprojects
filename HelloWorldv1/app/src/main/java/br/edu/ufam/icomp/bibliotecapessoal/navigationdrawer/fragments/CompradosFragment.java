package br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import br.edu.ufam.icomp.bibliotecapessoal.adapter.LivroAdapter;
import br.edu.ufam.icomp.bibliotecapessoal.util.RecyclerViewOnClickListenerHacker;
import br.edu.ufam.icomp.bibliotecapessoal.R;
import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Livro;
import br.edu.ufam.icomp.bibliotecapessoal.navigationdrawer.DrawerMainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompradosFragment extends Fragment implements RecyclerViewOnClickListenerHacker {

	private RecyclerView mRecyclerView;

	// §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ TESTE: PARA PREENCHER O RECYCLERVIEW (alterar para Usuario que conterá esta lista)
	private List<Livro> mListLivros;
	// §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_comprados, container, false);

		//// INFLATE RECYCLERVIEW
		mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
		mRecyclerView.setHasFixedSize(true); // o tamanho do RecyclerView não muda (otimização)
		mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) { super.onScrollStateChanged(recyclerView, newState); }

			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);

				LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
				LivroAdapter livroAdapter = (LivroAdapter) mRecyclerView.getAdapter();

				if(mListLivros.size() == linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1) { // se o tamanho da lista for igual a quantidade de itens exibidos, carregam-se mais items
					List<Livro> listLivros = ((DrawerMainActivity) getActivity()).getSetLivroList(10);
					for(int i=0; i < listLivros.size(); ++i)
						livroAdapter.addNewLivroItem( listLivros.get(i), mListLivros.size() );
				}

			}
		});
		mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(), mRecyclerView, this));


		//// GERENCIAR ITENS
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
		linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		mRecyclerView.setLayoutManager(linearLayoutManager);

		/// SET ADAPTER
		mListLivros = ((DrawerMainActivity) getActivity()).getSetLivroList(10); // §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ TESTE: quantidade
		LivroAdapter livroAdapter = new LivroAdapter(getActivity(), mListLivros);
//		livroAdapter.setRecyclerViewOnClickListenerHacker(this);
		mRecyclerView.setAdapter(livroAdapter);




		return view;
	}

	@Override
	public void onClickListener(View viewz, int position) {
		Toast.makeText(getActivity(), "Position: " + position, Toast.LENGTH_SHORT).show();
		/*
		LivroAdapter livroAdapter = (LivroAdapter) mRecyclerView.getAdapter();
		livroAdapter.removeLivroItem(position);
		*/
	}
	@Override
	public void onLongPressClickListener(View view, int position) {
		Toast.makeText(getActivity(), "Position: " + position, Toast.LENGTH_SHORT).show();
		LivroAdapter livroAdapter = (LivroAdapter) mRecyclerView.getAdapter();
		livroAdapter.removeLivroItem(position);
	}

	/////// INNER CLASS
	private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {
		private Context mContext;
		private GestureDetector mGestureDetector;
		private RecyclerViewOnClickListenerHacker mRecyclerViewOnClickListenerHacker;

		public RecyclerViewTouchListener(Context context, final RecyclerView recyclerView, final RecyclerViewOnClickListenerHacker recyclerViewOnClickListenerHacker){
			mContext = context;
			mRecyclerViewOnClickListenerHacker = recyclerViewOnClickListenerHacker;

			mGestureDetector = new GestureDetector(mContext, new GestureDetector.SimpleOnGestureListener(){
				@Override
				public void onLongPress(MotionEvent e) {
					super.onLongPress(e);

					View cv = recyclerView.findChildViewUnder(e.getX(), e.getY());
					if(cv != null && mRecyclerViewOnClickListenerHacker != null){
						mRecyclerViewOnClickListenerHacker.onLongPressClickListener(cv,
								recyclerView.getChildPosition(cv));
					}
				}

				@Override
				public boolean onSingleTapConfirmed(MotionEvent e) {
//					return super.onSingleTapConfirmed(e);
					View cv = recyclerView.findChildViewUnder(e.getX(), e.getY());
					if(cv != null && mRecyclerViewOnClickListenerHacker != null){
						mRecyclerViewOnClickListenerHacker.onClickListener(cv,
								recyclerView.getChildPosition(cv));
					}
					return true;
				}
			});

		}

		@Override
		public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
			mGestureDetector.onTouchEvent(e);
			return false;
		}

		@Override
		public void onTouchEvent(RecyclerView rv, MotionEvent e) {
		}

		@Override
		public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

		}
	}

}
