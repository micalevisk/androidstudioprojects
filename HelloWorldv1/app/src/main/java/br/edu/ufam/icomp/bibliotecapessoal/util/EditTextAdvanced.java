package br.edu.ufam.icomp.bibliotecapessoal.util;

import android.content.Context;
import android.text.InputFilter;
import android.widget.EditText;

/**
 * Um EditText com filtros adotados para as credenciais: sem brancos; tudo minúsculo
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 02/02/2017
 */
public class EditTextAdvanced extends EditText {
	public EditTextAdvanced(Context context) {
		super(context);
		InputFilter[] filtroCredenciais = new InputFilter[] { new InputFilterAllLowerNoSpaces() };
		this.setFilters(filtroCredenciais);
	}
}
