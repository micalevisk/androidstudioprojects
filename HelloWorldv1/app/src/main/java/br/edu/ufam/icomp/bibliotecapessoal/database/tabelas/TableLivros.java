package br.edu.ufam.icomp.bibliotecapessoal.database.tabelas;

import android.provider.BaseColumns;

import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 10/02/2017
 */
public class TableLivros implements BaseColumns {
	public static final String TABLE_NAME = "livros";

	public static final String USUARIOS_NICK	= "usuarios_nick";	// NN
	public static final String NOME				= "nome";			// NN
	public static final String AUTOR 			= "autor";			// NN
	public static final String DESCRICAO		= "descricao";
	public static final String EDICAO			= "edicao";			// NN
	public static final String CATEGORIA		= "categoria";		// NN
	public static final String DESEJADO			= "desejado";		// DEFAULT 0
	public static final String FAVORITO			= "favorito";		// DEFAULT 0
	public static final String DIA_DEVOLUCAO	= "dia_devolucao";


	public static final String QUERY_CREATE = String.format("CREATE TABLE '%s' ( '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER DEFAULT 0, '%s' INTEGER DEFAULT 0, '%s' TEXT )",
		Constantes.TABLE_LIVROS,
		Constantes.COL_USUARIOS_NICK,
		Constantes.COL_NOME,
		Constantes.COL_AUTOR,
		Constantes.COL_DESCRICAO,
		Constantes.COL_EDICAO,
		Constantes.COL_CATEGORIA,
		Constantes.COL_DESEJADO,
		Constantes.COL_FAVORITO,
		Constantes.COL_DIA_DEVOLUCAO);

}
