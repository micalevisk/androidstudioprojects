package br.edu.ufam.icomp.bibliotecapessoal.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Html;

import br.edu.ufam.icomp.bibliotecapessoal.R;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class Constantes {
	public static final String APP_NAME = "Hello_Micael";


	//////////////////// [ CHAVES PARA SALVAR PREFERÊNCIAS COMPARTILHADAS E PASSAR DADOS PARA INTENT ] ////////////////////
	public static final String FILE_KEY_MAIN_ACTIVITY = "MainActivity";
	public static final String CHECKBOX_MANTERCONEXAO = "cbxManterConexao";
	public static final String ULTIMO_LOGADO = "ultimoUsuarioLogado";

	//////////////////// [ CHAVES PARA SALVAR DADOS NO BANCO SQLITE ] ////////////////////
	public static final String BDKEY_USERNAME = "nick";		// nome da coluna que guarda o login no BD
	public static final String BDKEY_PASSWORD = "senha";	// nome da coluna que guarda a senha no BD

	//////////////////// [ CONFIGS BANCO SQLITE ] ////////////////////
	public static final String DATABASE_LOG_TAG = "BD";
	/// Table Names
	public static final String TABLE_USUARIOS	= "usuarios";
	public static final String TABLE_LIVROS		= "livros";
	/// USUARIOS Table - column names
	public static final String COL_NICK		= "nick";	// PK, NN
	public static final String COL_SENHA	= "senha";	// NN
	/// LIVROS Table - column names
	public static final String COL_USUARIOS_NICK	= "usuarios_nick";	// NN
	public static final String COL_NOME				= "nome";			// NN
	public static final String COL_AUTOR 			= "autor";			// NN
	public static final String COL_DESCRICAO		= "descricao";
	public static final String COL_EDICAO			= "edicao";			// NN
	public static final String COL_CATEGORIA		= "categoria";		// NN
	public static final String COL_DESEJADO			= "desejado";		// DEFAULT 0
	public static final String COL_FAVORITO			= "favorito";		// DEFAULT 0
	public static final String COL_DIA_DEVOLUCAO	= "dia_devolucao";


	//////////////////// [ MÉTODOS ESTÁTICOS ] ////////////////////
	public static void apagarTodasSharedPreferences(Context context, String key){
		context.getSharedPreferences(key, context.MODE_PRIVATE).edit().clear();
	}


	public static AlertDialog initDialogSobre(Context context){
		return new AlertDialog.Builder(context)
				.setTitle(context.getString(R.string.titleSobre))
				.setMessage( Html.fromHtml(context.getString(R.string.messageSobre)) )
				.setNeutralButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.setIcon(android.R.drawable.ic_menu_info_details)
				.create();
	}



}
