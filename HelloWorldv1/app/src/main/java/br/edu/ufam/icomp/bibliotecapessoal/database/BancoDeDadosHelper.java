package br.edu.ufam.icomp.bibliotecapessoal.database;

import java.security.*;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import br.edu.ufam.icomp.bibliotecapessoal.util.Constantes;

/**
 * Manipula dados mais gerais do BD. C.R.U.D.
 *
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class BancoDeDadosHelper extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = Constantes.APP_NAME + ".db";


	/// Table Create Statements
	private static String CREATE_TABLE_USUARIOS; //= "CREATE TABLE "+Constantes.TABLE_USUARIOS+" ( "+Constantes.COL_NICK +" TEXT PRIMARY KEY NOT NULL, "+Constantes.COL_SENHA +" TEXT NOT NULL )";
	private static String CREATE_TABLE_LIVROS;

	public BancoDeDadosHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		CREATE_TABLE_USUARIOS = String.format("CREATE TABLE '%s' ( '%s' TEXT PRIMARY KEY NOT NULL, '%s' TEXT NOT NULL )", Constantes.TABLE_USUARIOS, Constantes.COL_NICK, Constantes.COL_SENHA);
		CREATE_TABLE_LIVROS = String.format("CREATE TABLE '%s' ( '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER DEFAULT 0, '%s' INTEGER DEFAULT 0, '%s' TEXT )",
				Constantes.TABLE_LIVROS, 
				Constantes.COL_USUARIOS_NICK, 
				Constantes.COL_NOME, 
				Constantes.COL_AUTOR, 
				Constantes.COL_DESCRICAO,
				Constantes.COL_EDICAO, 
				Constantes.COL_CATEGORIA, 
				Constantes.COL_DESEJADO,
				Constantes.COL_FAVORITO, 
				Constantes.COL_DIA_DEVOLUCAO);
	}

	public void fecharConexao() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen()) db.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// creating required tables
		db.execSQL(CREATE_TABLE_USUARIOS);
		db.execSQL(CREATE_TABLE_LIVROS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		if (oldVersion == newVersion) return;
		final String DROP_TABLE = "DROP TABLE IF EXISTS ";

		/// on upgrade drop older tables
		db.execSQL(DROP_TABLE + Constantes.TABLE_USUARIOS);
		db.execSQL(DROP_TABLE + Constantes.TABLE_LIVROS);

		// create new tables
		onCreate(db);
		Log.i("rastrear","fim onUpgrade");
	}


	// (c) http://stackoverflow.com/questions/3934331/how-to-hash-a-string-in-android
	// http://www.miraclesalad.com/webtools/md5.php
	public static String MD5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)	hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}


}
