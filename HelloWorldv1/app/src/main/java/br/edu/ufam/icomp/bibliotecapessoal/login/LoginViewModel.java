package br.edu.ufam.icomp.bibliotecapessoal.login;

import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.text.Editable;

import android.databinding.Bindable;
import android.text.InputFilter;
import android.util.Log;
import android.widget.EditText;

import br.edu.ufam.icomp.bibliotecapessoal.util.InputFilterAllLowerNoSpaces;
import br.edu.ufam.icomp.bibliotecapessoal.util.TextWatcherAdapter;
import br.edu.ufam.icomp.bibliotecapessoal.BR;

import br.edu.ufam.icomp.bibliotecapessoal.R;
import br.edu.ufam.icomp.bibliotecapessoal.databinding.ActivityMainBinding;


/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 21/01/2017
 */
public class LoginViewModel extends BaseObservable {

	Resources resources;

	private String username;
	private String password;

	private String loginMessage;

	private TextWatcherAdapter keyboardWatcher;
	private boolean camposValidos;

	private EditText etNick;
	private EditText etSenha;


	public LoginViewModel(Resources resources, final ActivityMainBinding activityMainBinding){
		this.etNick = activityMainBinding.etNick;
		this.etSenha = activityMainBinding.etSenha;
		this.resources = resources;

		final InputFilter[] filtroCredenciais = new InputFilter[] { new InputFilterAllLowerNoSpaces() };
		etNick.setFilters(filtroCredenciais);
		etSenha.setFilters(filtroCredenciais);

		this.keyboardWatcher = new TextWatcherAdapter() {
			@Override public void afterTextChanged(Editable s) {
				verificarCampos();
				notifyPropertyChanged(BR.username);
			}
		};
	}


	////////////////////////////// [ EXTRAS ] //////////////////////////////
	@Bindable
	public TextWatcherAdapter getKeyboardWatcher(){
		return keyboardWatcher;
	}

	@Bindable
	public boolean isCamposValidos(){
		return camposValidos;
	}


	////////////////////////////// [ CREDENTIALS ] //////////////////////////////
	@Bindable
	public String getUsername(){
		return username;
	}

	@Bindable
	public String getPassword(){
		return password;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public void setPassword(String password){
		this.password = password;
	}


	private boolean camposInicializados(){
		return !(username == null || password == null);
	}


	////////////////////////////// [ INFORMATIONS ] //////////////////////////////
	@Bindable
	public String getLoginMessage(){
		return loginMessage;
	}


	////////////////////////////// [ WIDGETS NA ACTIVITY PRINCIPAL ] //////////////////////////////
	public void reset(){
		Log.i("rastrear","reset");
		etNick.setText(null);
		etSenha.setText(null);

		/// init estados iniciais (configs salvas da tela de login)
		etNick.requestFocus();
	}


	////////////////////////////// [ HELPERS ] //////////////////////////////
	private void verificarCampos(){
		if(camposInicializados()){
				camposValidos = !username.trim().isEmpty() && !password.trim().isEmpty();
				notifyPropertyChanged(BR.camposValidos);
		}
	}


	public void loginSucceeded(){
		loginMessage = resources.getString(R.string.strSaudacao);
		loginMessage = String.format("%s, %s!", loginMessage, username);
		notifyPropertyChanged(BR.loginMessage);
	}

	public void loginFailed(){
		loginMessage = resources.getString(R.string.strErroLogin);
		notifyPropertyChanged(BR.loginMessage);
	}

}