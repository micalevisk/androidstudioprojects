package br.edu.ufam.icomp.bibliotecapessoal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import br.edu.ufam.icomp.bibliotecapessoal.R;
import br.edu.ufam.icomp.bibliotecapessoal.database.bean.Livro;
import br.edu.ufam.icomp.bibliotecapessoal.util.RecyclerViewOnClickListenerHacker;


/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 10/02/2017
 */
public class LivroAdapter extends RecyclerView.Adapter<LivroAdapter.MyViewHolder> {

	private List<Livro> mListLivros;
	private LayoutInflater mLayoutInflater;
	private RecyclerViewOnClickListenerHacker mRecyclerViewOnClickListenerHacker;

	public LivroAdapter(Context context, List<Livro> listLivros){
		this.mListLivros = listLivros;
		this.mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}



	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) { // chamado quando há necessidade de criar uma nova view
		Log.i("rastrear", "onCreateViewHolder");
		View view = mLayoutInflater.inflate(R.layout.item_livro, parent, false);
		MyViewHolder myViewHolder = new MyViewHolder(view);
		return myViewHolder;
	}

	@Override
	public void onBindViewHolder(MyViewHolder holder, int position) { // vincula os dados da lista à view
		Log.i("rastrear", "onBindViewHolder");
		holder.tvNomeLivro.setText(mListLivros.get(position).getNome());
		holder.tvAutorLivro.setText(mListLivros.get(position).getAutor());
		holder.tvDescricaoLivro.setText(mListLivros.get(position).getDescricao());
	}

	@Override
	public int getItemCount() {
		return mListLivros.size();
	}

	/// TODO: 10/02/2017 atualizar no BD e na lista do usuário (se esta já não for uma referência para tal)
	public void addNewLivroItem(Livro livro, int position){
		mListLivros.add(livro);
		notifyItemInserted(position);
	}
	public void removeLivroItem(int position){
		mListLivros.remove(position);
		notifyItemRemoved(position);
	}


	/////// INNER CLASS
	public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
		public TextView tvNomeLivro, tvAutorLivro, tvDescricaoLivro;

		public MyViewHolder(View itemView){
			super(itemView);

			this.tvNomeLivro = (TextView) itemView.findViewById(R.id.tvNomeLivro);
			this.tvAutorLivro = (TextView) itemView.findViewById(R.id.tvAutorLivro);
			this.tvDescricaoLivro = (TextView) itemView.findViewById(R.id.tvDescricaoLivro);

			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if(mRecyclerViewOnClickListenerHacker != null){
				mRecyclerViewOnClickListenerHacker.onClickListener(v, getPosition());
			}
		}
	}


	//////////////////////// [ SETTERS ] ////////////////////////

	public void setRecyclerViewOnClickListenerHacker(RecyclerViewOnClickListenerHacker mRecyclerViewOnClickListenerHacker) {
		this.mRecyclerViewOnClickListenerHacker = mRecyclerViewOnClickListenerHacker;
	}



}
