package br.edu.ufam.icomp.bibliotecapessoal.util;

import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 23/01/2017
 */
public class StringUtils {

	private static final Pattern p = Pattern.compile("\\b\\w{3,}\\b");

	/**
	 * A cada três caracteres da cadeia, executa um {@link String#toUpperCase()} no primeiro caractere.
	 * @param text - O texto que será convertido.
	 * @return O texto com todos os primeiros caracteres (de uma palavra) em caixa-alta.
	 */
	public static String capitalize(String text) {
		Log.i("rastrear", "capitalize");
		Matcher m = p.matcher(text);

		StringBuilder sb = new StringBuilder();
		int last = 0;
		while (m.find()) {
			sb.append(text.substring(last, m.start()));
			String matches = m.group(0);
			String firstChar = (matches.charAt(0) + "").toUpperCase();
			sb.append(firstChar + matches.subSequence(1, matches.length()));
			last = m.end();
		}
		sb.append(text.substring(last));

		return sb.toString();
	}

}

// http://stackoverflow.com/questions/2770967/use-java-and-regex-to-convert-casing-in-a-string
// https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
// http://www.java2s.com/Tutorials/Java/Java_Regular_Expression/0070__Java_Regex_Groups.htm
