package br.edu.ufam.icomp.bibliotecapessoal.util;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;

/**
 * Filtro para usar no EditText afim de elimnar os brancos e o uso de caracteres maiúsculos.
 * @author Micael Levi &#8212; 21554923 &lt;mllc&#64;icomp.ufam.edu.br&gt;
 * @since 28/01/2017
 */
public class InputFilterAllLowerNoSpaces implements InputFilter {

	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		for (int i = start; i < end; i++) {
			if (Character.isWhitespace(source.charAt(i))) return ""; // remove o uso de espaços
			if(Character.isUpperCase(source.charAt(i))){ // converte maísculas para minúsculas
				char[] v = new char[end - start];
				TextUtils.getChars(source, start, end, v, 0);
				String s = new String(v).toLowerCase();

				if (source instanceof Spanned) {
					SpannableString sp = new SpannableString(s);
					TextUtils.copySpansFrom((Spanned) source, start, end, null, sp, 0);
					return sp;
				} else return s;
			}
		}
		return null;// keep original
	}
}
